import {Component, OnInit} from '@angular/core';
import {IProduct} from "./IProduct";


@Component({
    selector : 'pm-products',
    templateUrl : './product-list.component.html',
    styleUrls: ['./product-list.component.css']
})



export class ProductListComponent implements OnInit{
    pageTitle : string = "Product List";
    imageWidth : number = 50;
    imageMargin : number= 2;
    showImage : boolean = false;

    _listFilter : string = 'cart'; //named him with _ prefix so that the methods are called listFilter

    //changing data type any to an interface IProduct this time.
    products : IProduct[] = [ //any is a generic datatype 
        {
            "productId" : 2,
            "productName" : "Garden Cart",
            "productCode" : "GDN-0023",
            "releaseDate" : "2/4/2016",
            "description" : "15 gallon capacity rolling garden cart",
            "price" : 32.99,
            "starRating" : 4.2,
            "imageUrl" : "http://openclipart.org/image/300px/svg_to_png/58471/garden_cart.png"
        },
        {
            "productId" : 5,
            "productName" : "Hammer",
            "productCode" : "TBX-0048",
            "releaseDate" : "5/1/2016",
            "description" : "Curved claw steel hammer",
            "price" : 8.9 ,
            "starRating" :  4.8,
            "imageUrl" : "http://openclipart.org/image/300px/svg_to_png/73/rejon_Hammer.png"
        }
    ];

    //Initialised Array which will filter the products.
    filteredProducts : IProduct[];

    //getter
    get listFilter() : string {
        return this._listFilter;
    }

    //setter
    set listFilter(value : string){
            this._listFilter = value;
            this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter) : this.products;
    }

   
    constructor(){
        this.filteredProducts = this.products;
        this.listFilter = 'cart';
    }

    performFilter(filterBy : string) : IProduct[]{
        filterBy = filterBy.toLocaleLowerCase(); //converting value to lowercase
        return this.products.filter((product : IProduct) => product.productName.toLocaleLowerCase().indexOf(filterBy) !== -1);
    }

    toggleImage() : void{
        //qied chapter 4 nimla l method
        this.showImage = !this.showImage;
    }

    ngOnInit(){ //On component initilisation.
        console.log('In OnInIt');
    }

   
}

