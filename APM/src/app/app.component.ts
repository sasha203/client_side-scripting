import{Component} from '@angular/core';
//import { METHODS } from 'http';

@Component({
  selector : 'pm-root',
  templateUrl : './app.component.html',
  //styleUrls: ['./app.component.css']
})

export class AppComponent{
  pageTitle : string = 'Acme Product Management';  //interpolation is used to comunicate the pageTitle with the app.component.html

 //Creating a method can be called inside {{}} (interpolation)
  getTitle() : string{
    return this.pageTitle
  }

  //property binding are used for non-string data values over interpolation which is used only for strings.
  
}