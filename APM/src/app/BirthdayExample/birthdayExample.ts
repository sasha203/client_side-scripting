import{Component} from '@angular/core';




@Component({
    selector : 'birthday-example',
    templateUrl : './birthdayExample.html'
})




export class birthdayExampleComponent{
    birthday = new Date(1990,3,4);
    toggle : boolean = true;

    toggleFormat() { 
        this.toggle = !this.toggle; 
    }

    getFormat() { 
        return this.toggle ?  "shortDate" : "fullDate";
    } 

}


