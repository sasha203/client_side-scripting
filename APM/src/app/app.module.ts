import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { ProductListComponent } from '../Products/product-list.component';
import{birthdayExampleComponent} from './BirthdayExample/birthdayExample';
import{calculatorComponent} from './calculator/calculator';
import { convertToSpacesPipe } from '../Products/convertToSpacesPipe';



@NgModule({
  declarations: [
    AppComponent, ProductListComponent, birthdayExampleComponent, calculatorComponent, convertToSpacesPipe
  ],
  imports: [
    BrowserModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
